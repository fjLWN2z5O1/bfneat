
  # BFNEAT

 an object model and kotlin library for pragmatic gamification of universal function approximators via the NEAT genre.


 ### a breif summary of the "game" rules:

evolution and competitive selection is application specific.  not included here.

evaluation is done by Gnomes on thier Jeans.  Evaluation of each Jean occupies one Impulse

evaluation of Jeans happens in order from 0..n

there are 4 sources of impulse: 
    * Input
    * Bias
    * Hidden
    * Output

a set of weights exactly the size of Impulse array exists in each Jean

each jean keeps an ordered Array of 0 or more distinct Inputs.

Gnome initialization prevents links to Output Impulses

Gnome Output Jean initilaization can only link to Hidden Jeans

Gnome Hidden Jean initialization can not link to Output jeans

the global "Sim" object stores the size of INPUT, BIAS, HIDDEN, and OUTPUT as well as a RESERVED count of Hidden Jeans 
    which are left with no links and zero (0.0) weights.

Gnome Initialization never creates a link to Reserved hidden nodes. 

A Gnome has an Age measured by generations counter in the Sim object from origin generation.

The system has an asymettrically balanced assortment of slots which mutate a Gnome or Jean.  
 1. These are potentially destructive.
 2. The external process invoking Mutations should perform on Gnome clones (copy ctor) soas to create a pristine age measurement
    of a Gnome which is immutable by every other Kotlin means.
 3. Mutations follow seperate rules from initialization, not covered here; chiefly 1 above.

Impulse evaluation performs from 0..n in order
 * overwriting one Impulse at a time 
   *  any two links to the same impulse on opposite sides of that impulse  e.g.   1 -> _2_ and 3-> _2_ have no guarantees of seeing the same impulse value.
 * Initialization rules are Gnome-creation specific while Impulse _evaluation_ is performed without link direction bias nor RESERVE restrictions.



----------------
the clean_dep branch runs with no external deps

src/test/Xor.kt runs an evaluator showing the NEAT style genetic learning alg functionality

---

Activation functions borrowed from ... attribution inline

![AF Graph](image.png)

