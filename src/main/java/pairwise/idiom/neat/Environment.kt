package pairwise.idiom.neat

interface Environment {
    companion object {//antipatterns galore here
        lateinit var sim: Sim
        var linkGuidance: List<Int>?=null
         var linkHistogram: IntArray?=null
    }
}
