package pairwise.idiom.neat

import kotlin.math.absoluteValue
import kotlin.math.sqrt


/**
 * all credit to https://github.com/thunlp/THUTag
 *
 */
object Normalizer {
    fun l1Normalization(values: DoubleArray) {
        val size = values.size
        var i = 0
        var sum = 0.0

        while (i < size) {
            sum += values[i].absoluteValue
            i++
        }
        if (sum != 0.0) {
            i=0
            while (i < size) {
                values[i] = values[i] / sum
                i++
            }
        }
    }

    fun fL1Normalization(values: FloatArray) {
        val size = values.size
        var i = 0
        var sum = 0.0f

        while (i < size) {
            sum += values[i].absoluteValue
            i++
        }

        if (sum != 0.0f) {
            i = 0
            while (i < size) {
                values[i] = values[i] / sum
                i++
            }
        }
    }


    fun l2Normalization(values: DoubleArray) {
        var sum1: Double = 0.toDouble()
        var i = 0
        val size = values.size
        while (i < size) {
            val d = values[i].absoluteValue
            val d1 = values[i].absoluteValue
            sum1 += d * d1
            i++
        }
        i = 0
        val sum = sqrt(sum1)
        while (i < size) {
            values[i] = values[i] / sum
            i++
        }
    }

}