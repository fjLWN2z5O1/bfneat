package pairwise.idiom.neat

    data class Sim(
            @JvmField val INPUTS: Int,
            @JvmField val BIAS: Int,
            @JvmField val HIDDEN: Int,
            @JvmField val OUTPUTS: Int,
            @JvmField val POPULATION: Int,
            var CONN_DENSITY: Double,
            val RESERVED: Int,
    ) {
        @JvmField
        val inputRange = 0 until INPUTS
        @JvmField
        val biasRange = (inputRange.last.inc()).let { start -> start until (start + BIAS) }
        @JvmField
        val indirectHiddenRange = (biasRange.last.inc()).let { start -> start until (start + HIDDEN) }
        @JvmField
        val indirectOutputRange = (indirectHiddenRange.last .inc()).let { start -> start until (start + OUTPUTS) }
    }
