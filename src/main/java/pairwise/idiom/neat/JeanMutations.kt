package pairwise.idiom.neat

import pairwise.idiom.neat.ActivationFunction.Linear
import pairwise.idiom.neat.Environment.Companion.sim
import pairwise.idiom.neat.Jean.Companion.PHIf
import pairwise.idiom.neat.Jean.Companion.flip
import vec.macros.*
import vec.util._a
import vec.util.debug
import vec.util.indices
import vec.util.tof
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.math.min
import kotlin.random.Random
import kotlin.random.asKotlinRandom
import pairwise.idiom.neat.JeanMutations.Companion.chances  as chances1
import pairwise.idiom.neat.JeanMutations.Companion.ensureOutputLink  as ensureOutputLink1
import pairwise.idiom.neat.JeanMutations.Companion.targetRanges  as targetRanges1


@Suppress("UNINITIALIZED_ENUM_COMPANION_WARNING")
enum class JeanMutations(val fraction: Int, val op: (Jean, Int, Gnome) -> Unit) {
    flipGate(15, { jean, jx, g ->
        if (sim.indirectHiddenRange.first + jx in sim.indirectOutputRange)
            chances1.random().op(jean, jx, g)
        else
            jean.accum = jean.accum.xor(jean.accum)
    }),
    changeAf(17, { jean, jx, g ->
        if (sim.indirectHiddenRange.first + jx in sim.indirectOutputRange)
            chances1.random().op(jean, jx, g)
        else
            jean.af = ActivationFunction.cached.random()
    }),

    perturbWeight(1670, { jean, _, _ ->
        repeat(kotlin.math.max(1, sim.run { indirectOutputRange.last } / 10)) {
            val rindex = jean.weights.indices.random()
            val value = jean.weights[rindex]
            jean.weights[rindex] = (tof(value).takeIf { it != 0.0f }
                    ?: current().nextDouble(-kotlin.math.PI, kotlin.math.PI)).toFloat() * (tof(current().nextFloat(-PHIf, PHIf)))
        }
    }),

    /**
     * flip 1-2 inputs
     */
    flipConnectionBits(22, { jean, _, g ->
        val x = jean.inputs.toSortedSet()
        val current = current()
        repeat(current.nextInt(1, 2)) {
            x.flip(current.nextInt(sim.indirectOutputRange.first))
        }
        jean.inputs = x.toIntArray()
        g.pruned = null
    }),

    /**
     * add 1 inputs
     */
    enrichConnections(24, { jean, _, g ->
        val ss = jean.inputs.toSortedSet()
        val x = ss.size
        val upperBound = sim.indirectOutputRange.first
        var t = -1
        if (x < upperBound) {
            while (x == ss.size) ss += current().nextInt(upperBound).also { t = it }
            jean.inputs = ss.toIntArray()
            jean.weights.apply { if (tof(this[t]) == 0f) this[t] = 1f }
        }
        g.pruned = null
    }),

    /**
     * rm 1 inputs
     */
    reduceConnections(47, { jean, _, g ->
        val x = jean.inputs.size
        if (x != 0)
            jean.removeInput(jean.inputs[current().nextInt(x)], g)
        g.pruned = null

    }),

    buildRecall(1, { _, jx, g: Gnome ->

        val jx1 = min(jx, sim.indirectOutputRange.first - 2)
        val start = sim.indirectHiddenRange.first + jx1
        for (i in start until min(start + 30, (sim.indirectOutputRange.first - 2))) {
            val j = g.jeans[i - sim.indirectHiddenRange.first]
            j.accum = false
            j.af = Linear
            j.weights[i + 1] = 1f
            j.inputs = intArrayOf(i + 1)
        }
    }),


    /**
     * reduce weights uniformly and pluck one link from the target range
     */
    darkenOuterGroup(12, { jean, jx, g: Gnome ->

        val impulseNumber = jx + sim.indirectHiddenRange.first
        val target = targetRanges1.filter { impulseNumber !in it }.random()
        for (x in target) {
            var n = jean.weights[x]
            if (0f == tof(n))
                n = current().nextFloat(-.167f, 0f)
            jean.weights[x] = tof(n / 2f)
        }
        val newlink = target.random() //.debug { it -> vec.util.logDebug { pairwise.idiom.neat.JeanMutations.darkenOuterGroup.name + " darkened $impulseNumber and nixed $it from range $target " } }
        jean.removeInput(newlink, g)
        g.pruned = null

    }),
    brightenOuterGroup(8, { jean, jx, g: Gnome ->

        val impulseNumber = jx + sim.indirectHiddenRange.first
        val intRange = targetRanges1.filter { impulseNumber !in it }.random()
        for (x in intRange) {

            var n = jean.weights[x]
            if (tof(n) == 0f)
                n = current().nextFloat(.167f)
            jean.weights[x] = tof(PHIf * n)
        }
        val newlink = intRange.random()
        jean.addInput(newlink, g)


    }),

    /**links to all except self*/
    GrandVizier(1, { jean, jx, g ->
        if (sim.indirectHiddenRange.first + jx in sim.indirectOutputRange)
            chances1.random().op(jean, jx, g)
        else {
            jean.inputs = ((0..sim.indirectHiddenRange.last).indices - jx).toIntArray()
            ensureOutputLink1(g, current().asKotlinRandom(), jx)
        }
        g.pruned = null
    }),

    /**
     * normalize a random number of random jeans
     * */
    L1norm(50, { _: Jean, _, g ->
        val count = current().nextInt(g.jeans.size)
        val backtrack = mutableSetOf(count.dec())
        repeat(count) {
            var x: Int
            do {
                x = current().nextInt(sim.HIDDEN + sim.OUTPUTS)
            } while (x in backtrack)
            backtrack += x
            Normalizer.fL1Normalization(g.jeans[x].weights)

        }

    }),
    fullAggregate(1, { _: Jean, _, g ->
        val random = current().asKotlinRandom()
        val line1 = 0..sim.indirectHiddenRange.last
        val indices = g.jeans.indices
        var ex: Int

        for (i in line1) {
            var attemptsRemaining = sim.INPUTS
            while (attemptsRemaining-- > 0) {
                ex = indices.random(random)
                if (i != ex) {
                    val jean1 = g.jeans[ex]
                    jean1.addInput(i, g)

                    ensureOutputLink1(g, random, ex)
                    break
                }
            }
        }

        g.pruned = null
    }),
    Orthodox(33, { _: Jean, _: Int, g: Gnome ->


        val pruned = g.pruned!!
        for (jx in pruned.`➤`.filter { it in sim.indirectHiddenRange })
            g.jeans[jx].apply { inputs = inputs.filter { it <= sim.biasRange.last }.toIntArray() }
        for (jx in pruned.`➤`.filter { it in sim.indirectOutputRange })
            g.jeans[jx].apply { inputs = inputs.filter { it in sim.biasRange.first..sim.indirectHiddenRange.last }.toIntArray() }
        g.pruned = null


    })
    ;

    companion object {


        @JvmStatic
        fun ensureOutputLink(g: Gnome, random: Random, ex: Int) {

            val oj = g.jeans[random.nextInt(sim.OUTPUTS) + sim.HIDDEN]

            val ss = oj.inputs.toSortedSet()
            if (ss.add(ex)) oj.inputs = ss.toIntArray()
        }

        @JvmStatic
        val targetRanges by lazy {
            _a[
                    sim.inputRange,
                    sim.biasRange,
                    sim.indirectHiddenRange,
                    sim.indirectOutputRange,
            ]
        }

        @JvmStatic
        val cached = values()

        /**
         * call chances.random() for a mutation
         */
        @JvmStatic
        val chances: List<JeanMutations> by lazy { combine(cached α { mutation -> mutation.fraction t2 { mutation } }).toList().debug { it.random() } }
    }
}

