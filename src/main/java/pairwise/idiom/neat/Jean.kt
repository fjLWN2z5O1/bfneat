package pairwise.idiom.neat

import pairwise.idiom.neat.Environment.Companion.sim
import vec.macros.get
import vec.macros.toList
import vec.macros.toVect0r
import vec.macros.α
import java.util.*
import kotlin.math.PI
import kotlin.math.max
import kotlin.math.sqrt
import java.util.concurrent.ThreadLocalRandom as Random


open class Jean(
        /**
         * does not clear the value if a hidden node.
         */
        @JvmField var accum: Boolean = Random.current().nextInt(0, 9) == 0,
        @JvmField var af: ActivationFunction = ActivationFunction.Linear,
        @JvmField var inputs: IntArray = hiddenLinksInit(),
        @JvmField val weights: FloatArray = weightsInit(),
) : Cloneable {
    constructor(cloned: Jean) : this(
            cloned.accum,
            cloned.af,
            cloned.inputs.clone(),
            cloned.weights.clone()
    )

    override fun toString(): String {
        return "Jean(accum=$accum, af=$af ${
            inputs.let {
                "inputs=" + inputs.toList() + ", weights=" + weights.toVect0r()[inputs].α {
                    it.toString().replace("0.", ".").take(4)
                }.toList()
            }
        }"
    }

    public override fun clone(): Jean = Jean(accum, af, inputs.clone(), weights.clone())

    companion object {
        const val PHIf = (PI / 2.0).toFloat()


        /**
         * initializes links as if Output Nodes were never links.
         */
        fun hiddenLinksInit(reserved: Int = 0, isOutputSource: Boolean = false): IntArray {
            val top by lazy { sim.indirectHiddenRange.last - reserved }
            val validRange by lazy { ((if (isOutputSource) sim.indirectHiddenRange.first else 0)..top).toList() }
            val p1 = TreeSet<Int>()

            val reps: Int = (sim.CONN_DENSITY.toFloat() * sim.indirectOutputRange.first.toFloat()).toInt()
            repeat(max(1, reps)) {
                val ints = if (Random.current().nextInt(8) != 0) (Environment.linkGuidance ?: validRange)
                else validRange
                val nextInt: Int = ints.random(); p1.flip(nextInt)
            }
            return p1.toIntArray()
        }

        /**
         * initializes weights as if Output Nodes were never links.
         */
        fun weightsInit(reserved: Int = 0, isOutputSource: Boolean = false): FloatArray {
            val top = sim.indirectHiddenRange.last - reserved
            val validRange = 0..top

            val span = sim.indirectOutputRange.last
            val floatArray = FloatArray(span.inc()) { x ->
                var r = 0f
                if ((!isOutputSource || x in sim.indirectHiddenRange) && (reserved == 0 || x in validRange && x !in sim.indirectOutputRange)) {
                    val d: Double = sqrt(1.0 - sim.CONN_DENSITY) / span.toDouble()
                    r = (Random.current().nextDouble(-d, d)).toFloat()
                }
                r
            }
            return floatArray
        }

        @JvmStatic
        fun <E> MutableCollection<E>.flip(nextInt: E) {
            if (nextInt in this) remove(nextInt)
            else add(nextInt)
        }
    }

    open fun removeInput(input: Int, parent: Gnome) {
        val ss = inputs.toMutableList()
        val remove = ss.remove(input)
        if (remove) inputs = ss.toIntArray()

        parent.run {
            if (input in sim.indirectHiddenRange) pruned = null
        }
    }

    open fun addInput(input: Int, parent: Gnome) {
        val ss = inputs.toSortedSet()
        if (ss.add(input)) inputs = ss.toIntArray()
        parent.run {
            if (input in sim.indirectHiddenRange) pruned = null
        }
    }
}

