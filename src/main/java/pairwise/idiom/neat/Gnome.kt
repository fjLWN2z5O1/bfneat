@file:Suppress("NOTHING_TO_INLINE")

package pairwise.idiom.neat


import pairwise.idiom.neat.Environment.Companion.sim
import vec.macros.*
import vec.macros.Vect02_.left
import vec.util._v
import vec.util.indices
import java.util.*
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.ThreadLocalRandom
import kotlin.math.absoluteValue
import kotlin.math.max

@JvmField
val jeanRange = 0 until (sim.HIDDEN + sim.OUTPUTS)

@JvmField
val jeanOutputRange = sim.HIDDEN until sim.HIDDEN + sim.OUTPUTS

open class Gnome(
        //not my favorite thing to do, this holds only the hidden and output nodes on the whole numberline,
        // but that conserves space better for large input counts shared among all gnomes
        @JvmField val jeans: Array<Jean> = Array(jeanRange.count()) { x: Int ->
            val jean = when (x + sim.indirectHiddenRange.first) {
                in sim.indirectHiddenRange.first..(sim.indirectHiddenRange.last - sim.RESERVED) -> Jean(inputs = Jean.hiddenLinksInit(sim.RESERVED), af = ActivationFunction.cached.random(), weights = Jean.weightsInit(sim.RESERVED))
                in sim.indirectOutputRange -> Jean(accum = false, inputs = Jean.hiddenLinksInit(sim.RESERVED, true), weights = Jean.weightsInit(sim.RESERVED, true))
                else -> {
                    val reserved = sim.indirectOutputRange.last.inc()
                    val weights = Jean.weightsInit(reserved)
                    Jean(accum = false, inputs = IntArray(0), weights = weights)
                }
            }
            jean
        },
        @JvmField var impulse: FloatArray = ThreadLocalRandom.current().let { Random -> FloatArray(jeanRange.count()) { Random.nextFloat(1.1f) } },
) {
    constructor(parent: Gnome) : this(parent.jeans.clone(), impulse = parent.impulse.clone())

    var points: Float = Float.NEGATIVE_INFINITY

    fun backtrace(jx: Int, singleStep: Boolean = false): Set<Int> {
        val bag = mutableSetOf<Int>(jx)
        var addl: MutableSet<Int>? = null
        val blast: Int = sim.biasRange.last
        do {
            if (addl == null) addl = bag
            val tmp = mutableSetOf<Int>()
            for (jx in addl)
                tmp += jeans[jx].inputs.filter { it > blast }
            tmp -= bag
            addl = tmp
            bag += addl
        } while (addl!!.isNotEmpty() && !singleStep)
        return bag
    }

    fun normalizationSteps(jx1: Int):Vect02<IntArray, FloatArray> {
        val jxv = _v[jx1]

        val bag = mutableSetOf<Int>(jx1)

        val scanned = mutableListOf<Pai2<IntArray, FloatArray>>()
        val agenda = mutableListOf<Pai2<IntArray, FloatArray>>()
        var backtrace1: MutableSet<Int>

        var viter = jxv.iterator()
        do {
            val jx = viter.next()
            backtrace1 = backtrace(jx, true).toMutableSet()
            backtrace1 -= bag
            val backtrace: Vect0r<Int> = backtrace1.toVect0r()
            val ia: IntArray = backtrace.toIntArray()
            agenda += (ia t2 jeans[jx].weights.toVect0r()[ia].toFloatArray())
            bag += backtrace1

            if (!viter.hasNext()) {
                val left = combine(agenda.toVect0r().left α IntArray::toVect0r)
                scanned += agenda
                agenda.clear()
                viter = left.iterator()
            }
        } while (  viter.hasNext())
        return scanned.toVect0r()
    }

    var pruned: Vect0r<Int>? = null
        get() = field ?: run {
            val inflection = max(sim.INPUTS / 2, 8)
            val bag: ConcurrentSkipListSet<Int> = ConcurrentSkipListSet(sim.indirectOutputRange.indices)
            val ub = sim.HIDDEN + sim.OUTPUTS
            val ofirst = sim.indirectOutputRange.first
            val hfirst = sim.indirectHiddenRange.first


            var full = false
            for (jx in sim.indirectOutputRange) {
                bag += jeans[jx - hfirst].inputs.filter { it in sim.indirectHiddenRange }
                full = bag.size == ub
                if (full) break
            }
            var addl = LinkedHashSet<Int>(ub)
            addl += bag.subSet(hfirst, sim.indirectHiddenRange.last)
            if (!full) do {
                val iterSet = LinkedHashSet<Int>(ub)
                for (jx in addl) {
                    val jean = jeans[jx - hfirst]
                    val inputs = jean.inputs
                    val isize = inputs.size
                    when {
                        isize > inflection -> {
                            var br = Arrays.binarySearch(inputs, hfirst)
                            if (0 > br) br = (br.absoluteValue).inc()
                            var t: Int = -1
                            while (br < isize && inputs[br].also { i: Int -> t = i } < ofirst) {
                                iterSet += t
                                br++
                            }
                        }
                        else -> {
                            iterSet += inputs.filter { it in sim.indirectHiddenRange }
                        }
                    }
                }
                addl = iterSet
                addl -= (bag)
                bag += (addl)
                full = bag.size == ub
            } while (!full && addl.isNotEmpty())

            if (full)
            /*sorted().*/ {
                (0 until ub).toVect0r()
            } else {
                val toIntArray = bag./*sorted().*/toIntArray()
                val toVect0r = toIntArray.toVect0r()

                toVect0r α { x: Int ->
                    val jx = x - hfirst
                    jx
                }
            }
        }.also { field = it  as Vect0r<Int>}as Vect0r<Int>

    @Transient
    var complexity: Int? = null
        get() = field ?: jeans.sumOf { it.inputs.size }.also { field = it }


    fun procreate(mate: Gnome): Gnome {
        val jeans1 = Array(jeanRange.count()) { i -> Jean(spinBottle(this, mate).jeans[i].clone()) }
        return Gnome(jeans1)
    }

    override fun toString(): String = "Gnome(points=$points, jeans=${jeans.toList()}"

    companion object {
        inline fun spinBottle(g1: Gnome, g2: Gnome): Gnome = if (ThreadLocalRandom.current().nextBoolean()) g1 else g2
        fun usefulScope(gnome: Gnome, index: Int) = gnome.jeans[index].inputs.filter {
            gnome.jeans[it].accum && index == it || it < index
        }.toList()
    }
}
