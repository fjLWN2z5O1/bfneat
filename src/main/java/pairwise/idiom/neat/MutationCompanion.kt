@file:Suppress("EnumEntryName")

package pairwise.idiom.neat

import pairwise.idiom.neat.Environment.Companion.sim
import vec.macros.t2
import vec.ml.featureRange
import vec.ml.normalize
import vec.util.todub
import vec.util.tof
import kotlin.math.PI
import kotlin.math.absoluteValue

private const val fPI = PI.toFloat()


public object MutationCompanion {
    private const val target = 9f / 8e0

    fun externNormalizeOutput(gnome: Gnome) {
        val outputs = jeanOutputRange.map { gnome.jeans[it] t2 gnome.impulse[it] }
        val (
                hotSpot,
                magnitude,
        ) = jeanOutputRange.fold(-1 t2 Double.NEGATIVE_INFINITY) { acc, ri ->
            val f = gnome.impulse[ri]
            val value = f.absoluteValue
            if (value > acc.second || !value.isFinite()) ri t2 todub(f)
            else acc
        }
        normalizeJean(hotSpot, gnome)
    }

    private fun normalizeJean(hotSpot: Int, gnome: Gnome) {
        var acc: Set<Int> = gnome.jeans[hotSpot].inputs!!.toSet()
        val tree = linkedSetOf<Int>()
        while (acc.isNotEmpty()) {
            tree += acc
            var z = IntArray(0)
            acc.filter { it in sim.indirectHiddenRange }.forEach {
                z += gnome.jeans[it - sim.indirectHiddenRange.first].inputs!!
            }
            acc = (z.toSet() - tree)
        }
        val nodeIndexes = tree.filter { it in sim.indirectHiddenRange }
        val wts = nodeIndexes.map { gnome.impulse[it - sim.indirectHiddenRange.first] }
        val fr = featureRange(wts)
        val divisor = (fr.second - fr.first) + Double.MIN_VALUE / target
        val margin = 1.0 / (divisor + Double.MIN_VALUE)
        for (it in nodeIndexes.filter { it in sim.indirectHiddenRange })
            gnome.jeans[it - sim.indirectHiddenRange.first].apply {
                for (wtIdx in inputs!!.filter { it in tree }) {
                    val orig = weights[wtIdx]
                    val adjustment = margin * fr.normalize(tof(orig))
                    weights[wtIdx] = tof(weights[wtIdx] * adjustment)
                }
            }
    }

    /**
     * clone wars reference.  replacable champion to clone
     */
    public var jangoFett = Gnome()
    public val theClones: Gnome
        get() {
            val jeans = jangoFett.jeans.clone()
            val gnome = Gnome(jeans.clone())
            val jx = (sim.indirectHiddenRange.first..sim.indirectOutputRange.last).random()
            val jean = gnome.jeans[jx]
            JeanMutations.chances.random().op(jean,jx ,gnome)
            return gnome
        }


    fun createHotSpot(g: Gnome, hotspot: Int) {
        val hs = g.jeans[hotspot - sim.indirectHiddenRange.first]
        sim.inputRange.forEach { hs.inputs=(hs.inputs+(it)).sortedArray() }
        sim.indirectOutputRange.forEach {
            (g.jeans[it - sim.indirectOutputRange.first]).also { it.inputs=(it.inputs+(hotspot)).sortedArray()   }
        }
        assert(g.jeans.size == jeanRange.count())
    }
}
