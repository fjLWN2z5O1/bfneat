package pairwise.idiom.neat

import kotlin.math.absoluteValue

/**
 *
 * main() will test sizes for a hidden node count.  for big counts, use a fixed heap -Xms and -Xmx together
 *
Pop:  POP: 1 HIDDEN: 1 (30038336528, 30064771072) <-  pop size: 8192k  jean@ 8192Kb
Pop:  POP: 1 HIDDEN: 10 (30038283072, 30064771072) <-  pop size: 8192k  jean@ 819Kb
Pop:  POP: 1 HIDDEN: 50     size: 16384k    jean@ 327Kb
Pop:  POP: 1 HIDDEN: 100    size: 16384k    jean@ 163Kb
Pop:  POP: 1 HIDDEN: 200    size: 16384k    jean@ 81Kb
Pop:  POP: 1 HIDDEN: 500    size: 16384k    jean@ 32Kb
Pop:  POP: 1 HIDDEN: 1000   size: 8192k     jean@ 8Kb
Pop:  POP: 1 HIDDEN: 2000   size: 32770k    jean@ 16Kb
Pop:  POP: 1 HIDDEN: 5000   size: 204839k   jean@ 40Kb
Pop:  POP: 1 HIDDEN: 7000   size: 393216k   jean@ 56Kb
Pop:  POP: 1 HIDDEN: 9000   size: 647238k   jean@ 71Kb
Pop:  POP: 1 HIDDEN: 10000  size: 802816k   jean@ 80Kb
Pop:  POP: 1 HIDDEN: 12000  size: 1163264k  jean@ 96Kb
Pop:  POP: 1 HIDDEN: 15000  size: 1799765k  jean@ 119Kb
Pop:  POP: 1 HIDDEN: 20000  size: 3201806k  jean@ 160Kb
Pop:  POP: 1 HIDDEN: 25000  size: 5027035k  jean@ 201Kb
Pop:  POP: 1 HIDDEN: 30000  size: 7405802k  jean@ 246Kb
Pop:  POP: 1 HIDDEN: 50000  size: 20913026k jean@ 418Kb


 */
internal class JeanTest {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val runtime = Runtime.getRuntime()
            val env = object : Environment {}
            val baseMax = runtime.maxMemory()


            repeat(100)
            {
                arrayOf(
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    50,
                    50,
                    50,
                    50,
                    50,
                    50,
                    100,
                    200,
                    500,
                    1_000,
                    2_000,
                    5_000,
                    7_000,
                    9_000,
                    10_000,
                    12_000,
                    15_000,
                    20_000,
                    25_000,
                    30_000,
                    50_000,
                    100_000
                ).distinct()/*.reversed()*/.onEach { HCOUNT ->
                    val rb: Long
                    val rk: Long
                    val tk: Double
                    also {


                        System.gc()



                        Thread.sleep(100)
                        val baseFree = runtime.freeMemory()
                        val baseTotal = runtime.totalMemory()
                        Environment.sim = Sim(
                                INPUTS = 0,
                                BIAS = 0,
                                HIDDEN = HCOUNT,
                                OUTPUTS = 0,
                                POPULATION = 1,
                                CONN_DENSITY = 0.00001,
                                RESERVED =  0,
                        )

                        val population = Population(Array<Gnome>(Environment.sim.POPULATION) { Gnome() }.toList())
                        rb  = (runtime.freeMemory()-baseFree.toDouble()).absoluteValue.toLong()
                        rk  = rb.absoluteValue / 1024
                        tk  = ((runtime.totalMemory()-baseTotal.toDouble()).absoluteValue) / 1024
                    }
                    println("Pop:  POP: ${Environment.sim.POPULATION} HIDDEN: ${Environment.sim.HIDDEN} ${runtime.freeMemory() to runtime.totalMemory()} <-  pop size: ${rk}k  jean@ ${rk / Environment.sim.POPULATION / Environment.sim.HIDDEN}Kb")

                }
                System.gc()
                Thread.sleep(10000)
            }


        }
    }
}
